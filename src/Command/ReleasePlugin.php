<?php

namespace Spip\Tools\Releases\Command;

use Spip\Tools\Releases\Config;
use Spip\Tools\Releases\Edit\Git;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Spip\Tools\Releases\Edit\Plugin;

class ReleasePlugin extends Release {

    protected $spip_version;
    protected $compat;

    protected function configure() {
        $this
            ->setName('release:plugin')
            ->setDescription('Prépare une release d’un plugin Core de SPIP')
            ->addArgument('plugin', InputArgument::REQUIRED, 'Nom du plugin')
            ->addArgument('branch', InputArgument::OPTIONAL, 'Branche à releaser', 'master')
            ->addOption('apply', null, InputOption::VALUE_NONE, 'Modifie, tag et commit les sources (sinon indique juste les changements qui seraient fait)')
            ->addOption('push', null, InputOption::VALUE_NONE, 'Push les modifications (apply ne push pas tout seul)')
            ->addOption('push-dry', null, InputOption::VALUE_NONE, 'Test le push les modifications uniquement')
            ->addOption('no-changelog', null, InputOption::VALUE_NONE, 'Ignore le test de changelog (probablement pratique pour une toute nouvelle branche).')
            ->addOption('no-checkout', null, InputOption::VALUE_NONE, 'Ne pas relancer checkout si le répertoire SPIP existe déjà.')
            ->addOption('spip', null, InputOption::VALUE_OPTIONAL, 'Version du SPIP à utiliser')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->init($input, $output);

        $this->io->title("Release Plugin");
        $this->askSpipVersion();

        $plugin = $input->getArgument('plugin');
        $branch = $input->getArgument('branch');
        $plugins = [$plugin => $branch];

        $this->releasePlugins($plugins);
        return COMMAND::SUCCESS;
    }

    protected function askSpipVersion() {
        $spip_version = $this->input->getOption('spip');
        $spip_version = $this->io->askQuestion(new Question("Pour quelle version SPIP  ?", $spip_version));

        $compats = [
            $this->generateCompatSpip($spip_version, 0, true),
            $this->generateCompatSpip($spip_version, 0),
            $this->generateCompatSpip($spip_version),
            'Autre',
        ];

        $compat = $this->io->askQuestion(new ChoiceQuestion("Quelle compat SPIP ?", $compats, 0));
        if ($compat === 'Autre') {
            $compat = $this->io->askQuestion(new Question("Quelle compat SPIP donc ?", $compats[1]));
        }
    
        $this->compat = $compat;
        $this->spip_version = $spip_version;
    }

    protected function releasePlugins($plugins) {
        foreach ($plugins as $plugin => $branch) {
            $this->io->title("Release $plugin $branch");
            $this->config = new Config($branch, $plugin);
            $this->release();
        }
    }


    protected function preparer() : bool {
        $name =  $this->config->getPlugin();
        $this->io->section("Prepare Plugin $name");
        $plugin = new Plugin($this->config->getDir());

        return $this->preparerPluginWithRetry($plugin, $this->spip_version, $this->compat);
    }

    protected function push() : bool {
        $this->io->title("Push !");
        $ok = $this->io->askQuestion(new ConfirmationQuestion("Prêt ?"));
        if (!$ok) {
            return false;
        }

        $directory = $this->config->getDir();
        $this->io->section("Push " . $this->config->getPlugin() . "\n");
        $git = new Git($directory);
        return $this->pushGit($git);
    }
}
