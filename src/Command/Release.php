<?php

namespace Spip\Tools\Releases\Command;

use DateTime;
use Spip\Tools\Releases\Config;
use Spip\Tools\Releases\Edit\Git;
use Spip\Tools\Releases\Edit\Plugin;
use Spip\Tools\Releases\Edit\Spip;
use Spip\Tools\Releases\ReleaseException;
use Spip\Tools\Releases\Style;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Spip\Tools\Releases\Edit\Changelog;
use Spip\Tools\Releases\Edit\ChangelogTxt;

class Release extends Command {

    /** @var SymfonyStyle */
    protected $io;

    /** @var InputInterface */
    protected $input;

    /** @var Config */
    protected $config;


    protected function init(InputInterface $input, OutputInterface $output) {
        $this->input = $input;
        $io = $this->io = new Style($input, $output);
        setlocale(LC_ALL, 'fr_FR');
    }

    protected function release() {
        if (!is_dir($this->config->getDir())) {
            $this->checkout();
        } elseif (!$this->input->getOption('no-checkout')) {
            $this->checkout();
        } else {
            $this->io->text("Checkout ignoré.");
        }
        $ok = $this->preparer();
        if ($ok and (
            $this->input->getOption('push') 
            or $this->input->getOption('push-dry'))
        ) {
            $this->push();
        }
        $this->io->writeLn("");
    }

    protected function checkout() {
        $destination = $this->config->getDir();
        $branch = $this->config->getBranchName();
        $plugin = $this->config->getPlugin();
        if ($plugin === 'spip') {
            $this->io->section("Checkout SPIP @$branch in $destination");
            $cmd = "checkout spip -b$branch git@git.spip.net $destination";
        } else {
            $this->io->section("Checkout $plugin @$branch in $destination");
            $cmd = "checkout git -b$branch git@git.spip.net:spip/$plugin.git $destination"; 
        }
        passthru($cmd);
    }

    protected function preparer() : bool {
        // a implementer en dépendance
        $this->io->error("preparer() not implemeted !");
        return false;
    }

        protected function push() : bool {
        // a implementer en dépendance
        $this->io->error("push() not implemeted !");
        return false;
    }


    protected function generateCompatSpip(string $spip_version, ?int $z = null, bool $star_y = false) : string {
        $v = explode('.', explode('-', $spip_version)[0]);
        $version_min = ($z === null) ? $spip_version : "{$v[0]}.{$v[1]}.$z";
        $version_max = "{$v[0]}.{$v[1]}.*";
        if ($star_y) {
            $version_max = "{$v[0]}.*";
        }
        return "[$version_min;$version_max]";
    }

    protected function preparerPluginWithRetry(Plugin $plugin, string $spip_version, string $compat) : bool {
        $ok = $this->preparerPlugin($plugin, $spip_version, $compat);
        if (!$ok) {
            do {
                $retenter = $this->io->askQuestion(new ConfirmationQuestion("Retenter ce plugin ?"));
                if ($retenter) {
                    $ok = $this->preparerPlugin($plugin, $spip_version, $compat);
                }
            } while ($retenter and !$ok);
        }
        if (!$ok) {
            throw new ReleaseException("Des erreurs à corriger empêchent la poursuite...");
        }
        return $this->io->askQuestion(new ConfirmationQuestion("Fin de la préparation. Continuer ?"));
    }

    protected function preparerPlugin(Plugin $plugin, string $spip_version, string $compat) : bool {
        $directory = $plugin->getDirectory();
        $name = basename($directory);
        $this->io->section("Plugin : $name");

        $errs = [];

        $git = $plugin->getGit();

        $this->io->text("Versions actuelles :");
        $paquet_version = $plugin->getPaquetVersion();
        $paquet_etat = $plugin->getPaquetEtat();
        $paquet_compat = $plugin->getPaquetCompat();
        $tag_paquet_version = $plugin->getPluginTag($paquet_version, true);

        $this->io->text("- version : <comment>$paquet_version</comment>");
        $this->io->text("- compat : <comment>$paquet_compat</comment>");

        if ($compat && $paquet_compat != $compat) {
            $this->io->fail("Attendu : <comment>$compat</comment>");
            $compats = [$compat, $paquet_compat, 'Autre',];
            $new_compat = $this->io->askQuestion(new ChoiceQuestion("Quelle compat SPIP ?", $compats, 0));
            if ($new_compat === 'Autre') {
                $new_compat = $this->io->askQuestion(new Question("Quelle compat SPIP donc ?", $compats[1]));
            }

            if ($paquet_compat != $new_compat) {
                $apply = $this->input->getOption('apply');
                if ($apply) {
                    $ok = $plugin->setPaquetCompat($new_compat);
                    if (!$ok) {
                        throw new ReleaseException("Erreur sur la modification du paquet.xml");
                    }
                    $sha = $git->sha();
                    $git->add('paquet.xml');
                    $git->commit("Compat $new_compat");
                    $this->io->test($sha !== $git->sha(), "Version $new_compat commitée");
                    if ($sha === $git->sha()) {
                        throw new ReleaseException("Erreur sur le commit du paquet.xml");
                    }
                } else {
                    $this->io->care("[Simulation] Compat en <comment>$new_compat</comment>");
                    $this->io->care("[Simulation] Commit la compat <comment>$new_compat</comment>");
                }
            }
        } else {
            $this->io->check("compat : <comment>$paquet_compat</comment>");
        }

        $this->io->text("\nTag présents sur HEAD :");
        $tags = $git->listTagsOnCommit();
        if ($tags) {
            foreach ($tags as $tag) {
                if ($tag === $paquet_version) {
                    $this->io->check("Tag : <comment>$tag</comment>");
                } else {
                    $this->io->text("- Tag : <comment>$tag</comment>");
                }
            }
        } else {
            $this->io->fail("Aucun");
        }

        $diff = $git->diff();
        if ($diff) {
            $this->io->text("\n<comment>Diff sur le git !</comment>");
            $this->io->text($diff);
            $errs[] = "Des modifications non commitées sont présentes.";
        }
        if ($errs) {
            $this->io->error($errs);
            return false;
        }

        // On pose un tag de version du plugin.
        // et sinon incrémenter la version du plugin et commiter.
        $is_dev = (strpos($paquet_version, '-dev') !== false);
        if (in_array($paquet_version, $tags)) {
            $tag_paquet_version = $paquet_version;
        }
        if (!in_array($tag_paquet_version, $tags) or $is_dev) {
            // si pas de tag à la version du paquet, on pourrait supposer que le numéro de version est un changement récent
            // mais en fait ça ne colle pas toujours... Alors… on propose d’incrémenter ou pas...
            if ($git->hasTag($tag_paquet_version)) {
                $this->io->fail("Un tag <comment>$tag_paquet_version</comment> antérieur existe.");
                $incrementer = true;
            } else {
                $this->io->care("Aucun tag <comment>$tag_paquet_version</comment> antérieur n’existe");
                $this->io->writeLn("");
                $this->io->text("<info>Logs -3 :</info>");
                $this->io->text(explode("\n", $git->lg("-3")));
                $this->io->text("<info>Logs -3 paquet.xml :</info>");
                $this->io->text(explode("\n", $git->lg("-3 paquet.xml")));
                $this->io->text("<info>URL :</info> https://git.spip.net/spip/" . $name);

                $identique = $git->log_first_sha() === $git->log_first_sha('paquet.xml');
                if ($is_dev) {
                    $this->io->check("Cette version est en dev. On publie une vraie version");
                    $incrementer = true;
                } elseif ($identique) {
                    $this->io->check("Cette version semble très récente, vous pouvez probablement la réutiliser (dernier commit identique).");
                    $incrementer = !$this->io->askQuestion(
                        new ConfirmationQuestion("Conserver cette version ? (sinon incrémenter).")
                    );
                } else {
                    $this->io->fail("Cette version n’est peut être pas très récente (dernier commit non identique).");
                    $incrementer = $this->io->askQuestion(
                        new ConfirmationQuestion("Incrémenter cette version ? (sinon conserver).")
                    );
                }
            } 
            $apply = $this->input->getOption('apply');
            if (!$incrementer) {
                if (!$this->verifyChangelog($plugin, $paquet_version)) {
                    $this->io->error("Erreur sur le CHANGELOG");
                    return false;
                } else {
                    if ($apply) {
                        $git->add('CHANGELOG.md');
                        $git->commit("docs(changelog): Release $paquet_version");
                    }
                }
                $this->createTag($git, $tag_paquet_version);
            } else {
                // sinon, on doit incrémenter la version du paquet.xml et commiter...
                $new_paquet_version = $plugin->incrementVersion($paquet_version);
                $tag_paquet_version = $plugin->getPluginTag($new_paquet_version, true);
                $ok = $this->io->askQuestion(new ConfirmationQuestion("Le paquet.xml va être incrémenté en '$new_paquet_version'. Continuer ?"));
                if (!$ok) {
                    $this->io->care("Opération bloquée par l’utilisateur.");
                    return false;
                }
                if ($apply) {
                    $ok = $plugin->setPaquetVersion($new_paquet_version);
                    if ($paquet_etat !== 'stable') {
                        $ok &= $plugin->setPaquetEtat('stable');
                    }
                    if (!$ok) {
                        $this->io->error("Erreur sur la modification du paquet.xml");
                        return false;
                    }
                    $sha = $git->sha();
                    $git->add('paquet.xml');
                    if (!$this->verifyChangelog($plugin, $new_paquet_version)) {
                        $this->io->error("Erreur sur le CHANGELOG");
                        return false;
                    } else {
                        $git->add('CHANGELOG.md');
                    }
                    $git->commit("build: Version $new_paquet_version");
                    $this->io->test($sha !== $git->sha(), "Version $new_paquet_version commitée");
                    if ($sha !== $git->sha()) {

                        $this->createTag($git, $tag_paquet_version);
                    } else {
                        $this->io->error("Erreur sur le commit du paquet.xml");
                        return false;
                    }
                } else {
                    $this->io->care("[Simulation] Increment version en <comment>$new_paquet_version</comment>");
                    if ($paquet_etat !== 'stable') {
                        $this->io->care("[Simulation] Passage Etat en <comment>stable</comment>");
                    }
                    $this->io->care("[Simulation] Commit la version <comment>$new_paquet_version</comment>");
                    if (!$this->verifyChangelog($plugin, $new_paquet_version)) {
                        $this->io->error("Erreur sur le CHANGELOG");
                        return false;
                    }
                     $this->createTag($git, $tag_paquet_version);
                }
            }
        }

        if ($errs) {
            $this->io->error($errs);
            return false;
        }

        return true;
    }

    protected function testTag(Git $git, string $tag) : ?string {
        if ($git->hasTag($tag)) {
            $this->io->care("Le tag <comment>$tag</comment> existe dans un autre commit que celui en cours.");
            return "Le tag $tag existe dans un autre commit que celui en cours.";
        } else {
            $this->io->check("Le tag <comment>$tag</comment> n’est pas encore utilisé.");
        }
        return null;
    }

    protected function createTag(Git $git, string $tag) {
        $apply = $this->input->getOption('apply');
        if ($apply) {
            $this->io->test($git->createTag($tag), "Création du tag <comment>$tag</comment>");
        } else {
            $this->io->care("[Simulation] Création du tag <comment>$tag</comment>");
        }
    }


    protected function pushGit(Git $git) : bool {
        $this->io->care("Vérifications (--dry-run)");
        $this->io->text("<info>Branche :</info> " . $git->currentBranch());
        $this->io->text("<info>Logs :</info>");
        $this->io->text(explode("\n", $git->lg_between()));
        $sha = $git->sha();
        $tag = $git->currentTagVersion();
        $need_push = $sha !== $git->sha("origin/" . $git->currentBranch());
        $need_tag = $tag !== $git->currentTagVersion("origin/" . $git->currentBranch());
        $this->io->test(!$need_push, "Commits à jour ($sha)");
        $this->io->test(!$need_tag, "Tags à jour ($tag)");
        if ($need_push) {
            $this->io->text("<info>Push :</info>");
            $this->io->text($git->push(true) ?? '');
        }
        if ($need_tag) {
            $this->io->text("<info>Push tags :</info>");
            $this->io->text($git->pushTags(true) ?? '');
        }

        if ($need_push || $need_tag) {
            if ($this->input->getOption('push-dry')) {
                $this->io->care("[Simulation] : on ne propose pas le vrai push ensuite.");
            } else {
                $ok = $this->io->askQuestion(new ConfirmationQuestion("On push pour de vrai ?"));
                if (!$ok) {
                    return false;
                }
                if ($need_push) {
                    $this->io->text("<info>Push :</info>");
                    $this->io->text($git->push() ?? '');
                }
                if ($need_tag) {
                    $this->io->text("<info>Push tags :</info>");
                    $this->io->text($git->pushTags() ?? '');
                }
            }
        }

        return $this->io->askQuestion(new ConfirmationQuestion("Continuer ?"));
    }

    protected function verifyChangelog(Plugin $plugin, string $version): bool {
        $errs = $this->checkChangelog($plugin, $version);
        if ($errs) {
            return false;
        }
        $apply = $this->input->getOption('apply');
        if ($apply) {
            $this->io->care("Changelog édité pour $version");
        } else {
            $this->io->care("[Simulation] Changelog pour $version");   
        }
        return true;
    }



    protected function checkChangelog(Plugin $plugin, string $version): array {

        $errs = [];
        $this->io->text("\nChangelog pour <info>$version</info>");
        $ignoreChangelog = $this->input->getOption('no-changelog');
        $changelog = $plugin->getExistantChangelog();

        if (!$changelog) {
            if (!$ignoreChangelog) {
                $errs[] = "Aucun fichier CHANGELOG.md (ou .TXT)";
            }
        } else {
            if ($changelog->hasChangelogVersion($version)) {
                $this->io->check("Le changelog a une indication pour cette version <info>$version</info>");
                if ($changelog->hasUnreleasedSection()) {
                    $this->io->fail("Le changelog contient une section <info>Unreleased</info>");
                    $errs[] = "Le CHANGELOG a une section 'Unreleased' en plus d’une section '$version'";
                } else {
                    $this->io->check("Le changelog ne contient pas de section 'Unreleased'");
                }
            } else {
                $this->io->fail("Le changelog n’est pas renseigné pour cette version <info>$version</info>");
                if ($changelog->hasUnreleasedSection()) {
                    $this->io->check("Le changelog contient une section <info>Unreleased</info>");
                } else {
                    $this->io->fail("Le changelog ne contient pas de section <info>Unreleased</info>");
                    $errs[] = "Le CHANGELOG n’a ni une section 'Unreleased', ni une section '$version'";
                }
                if (!$ignoreChangelog) {
                    $errs[] = "Le changelog n’est pas renseigné";
                }
            }
        }
        if ($errs) {
            $this->io->error($errs);
        }
        $choices = [
            'Non',
            'Oui, avec les commits',
            'Oui, sans les commits',
        ];
        $show = $this->io->askQuestion(new ChoiceQuestion("Monter un changelog ?", $choices, 1));
        $show = array_search($show, $choices);
        if ($show > 0) {
            $this->proposerChangelog($plugin, $version, $show == 1);
        }

        if (
            $changelog 
            && !$ignoreChangelog 
            && !$changelog->hasChangelogVersion($version)
            && $changelog->hasUnreleasedSection()
        ) {
            $unreleased_to_version = $this->io->askQuestion(new ConfirmationQuestion("Remplacer <info>Unreleased</info> par cette version <info>$version</info> ?"));
            if ($unreleased_to_version) {
                $apply = $this->input->getOption('apply');
                if ($apply) {
                    if ($changelog->setUnreleasedToVersion($version)) {
                        // a priori c’est que tout va bien
                        $errs = [];
                    };
                } else {
                    $this->io->care("[Simulation] Changelog modifié Unreleased -> $version");
                    // a priori c’est que tout va bien
                    $errs = [];
                }
            }
        }

        return $errs;
    }


    protected function proposerChangelog(Plugin $plugin, string $version, bool $show_commits = true) {

        $git = $plugin->getGit();
        $changelog = $plugin->getExistantChangelog() ?? $plugin->getChangelog();
        

        $version_precedente = $plugin->tryDecrementVersion($version);
        $version_precedente = $this->io->askQuestion(new Question("Quelle est la version précédente ?", $version_precedente));
        if (!$version_precedente) {
            $this->io->fail("Ignoré car aucune version précédente indiquée.");
            return;
        }
 
        $last_tag = $plugin->trouverTagPrecedent($version_precedente);
        if (!$last_tag) {
            $this->io->fail(sprintf(
                'Ignoré le tag %s ou %s n’existe pas.', 
                $plugin->getPluginTag($version_precedente, false), 
                $plugin->getPluginTag($version_precedente, true)
            ));
        }

        if ($plugin instanceof Spip) {
            $text = $changelog->forSpip($version_precedente, $version, $plugin, $this->config->getPluginsPath(), $show_commits);
            $this->io->text($text);
        } else {
            $text = $changelog->forPlugin($version_precedente, $version, $plugin, $show_commits);
            $this->io->text($text);
        }
        $this->io->writeLn("\n");



        $this->io->note(
            "Vous devez éditer " . basename($changelog->getPathChangelog()) . " en adaptant ce contenu proposé. "
            . "Il faut commiter la modification (pas besoin de pusher), mais... "
            . "Il faut que le tag de version n’ait pas encore été posé,"
            . " ou qu’il soit sur le même commit (HEAD) non pushé (commiter avec --amend)"
            . "\n\n"
            . "code " . $changelog->getPathChangelog()
            . "\n\n"
        );

        $tag_version = $plugin->getPluginTag($version, $last_tag['prefix']);
        $tag_version_alter = $plugin->getPluginTag($version, !$last_tag['prefix']);
        if ($git->hasTag($tag_version)) {
            $this->io->warning("Le tag $tag_version existe. Attention en ajoutant le changelog à ne pas créer de nouveau commit.");
        } elseif ($git->hasTag($tag_version_alter)) {
            $this->io->warning("Le tag $tag_version_alter existe (prefix différent!). Attention en ajoutant le changelog à ne pas créer de nouveau commit.");
        }

        $this->io->askQuestion(new Question("Mise en pause..."));
    }
}
