<?php

namespace Spip\Tools\Releases\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ReleasePluginsCore extends ReleasePlugin {

    protected $liste = [
        'master' => [
            'breves' => 'master',
            'dev' => 'master',
            'grenier' => 'master',
            'jquery_ui' => 'master',
            'petitions' => 'master',
            'organiseur' => 'master',
            'squelettes_par_rubrique' => 'master',
        ],
        '4.2' => [
            'breves' => '3.1',
            'dev' => '2.1',
            'grenier' => '2.0',
            'jquery_ui' => '1.15',
            'petitions' => '3.0',
            'organiseur' => '3.1',
            'squelettes_par_rubrique' => '2.0',
        ],
        '4.1' => [
            'breves' => '3.0',
            'dev' => '2.0',
            'grenier' => '2.0',
            'jquery_ui' => '1.15',
            'petitions' => '3.0',
            'organiseur' => '3.1',
            'squelettes_par_rubrique' => '2.0',
        ],
        '4.0' => [
            'breves' => '2.0',
            'dev' => '1.0',
            'grenier' => '1.0',
            'jquery_ui' => '1.14',
            'petitions' => '2.0',
            'organiseur' => '2.0',
            'squelettes_par_rubrique' => '1.4',
            'vertebres' => '1.5',
        ],
        '3.2' => [
            'dev' => 'spip-3.2',
            'grenier' => 'spip-3.2',
            'msie_compat' => 'spip-3.2',
        ],
    ];


    protected function configure() {
        $this
            ->setName('release:plugins-core')
            ->setDescription('Prépare une release des plugins Core de SPIP')
            ->addArgument('branch', InputArgument::OPTIONAL, 'Branche à releaser', 'master')
            ->addOption('apply', null, InputOption::VALUE_NONE, 'Modifie, tag et commit les sources (sinon indique juste les changements qui seraient fait)')
            ->addOption('push', null, InputOption::VALUE_NONE, 'Push les modifications (apply ne push pas tout seul)')
            ->addOption('push-dry', null, InputOption::VALUE_NONE, 'Test le push les modifications uniquement')
            ->addOption('no-checkout', null, InputOption::VALUE_NONE, 'Ne pas relancer checkout si le répertoire SPIP existe déjà.')
            ->addOption('no-changelog', null, InputOption::VALUE_NONE, 'Ignore le test de changelog (probablement pratique pour une toute nouvelle branche).')
            ->addOption('spip', null, InputOption::VALUE_OPTIONAL, 'Version du SPIP à utiliser')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->init($input, $output);

        $this->io->title("Release Plugin-Core");

        $branch = $input->getArgument('branch');
        $plugins = $this->liste[$branch] ?? null;
        if ($plugins === null) {
            $this->io->error("No plugins defined for branch $branch");
        }

        $this->askSpipVersion();
        $this->releasePlugins($plugins);

        return COMMAND::SUCCESS;
    }


}
