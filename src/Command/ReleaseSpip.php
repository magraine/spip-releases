<?php

namespace Spip\Tools\Releases\Command;

use DateTime;
use Spip\Tools\Releases\Config;
use Spip\Tools\Releases\Edit\Git;
use Spip\Tools\Releases\Edit\Plugin;
use Spip\Tools\Releases\Edit\Spip;
use Spip\Tools\Releases\ReleaseException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Spip\Tools\Releases\Edit\Changelog;
use Spip\Tools\Releases\Edit\ChangelogTxt;

class ReleaseSpip extends Release {

    protected function configure() {
        $this
            ->setName('release:spip')
            ->setDescription('Prépare une release d’une branche de SPIP')
            ->addArgument('branch', InputArgument::OPTIONAL, 'Branche à releaser', 'master')
            ->addOption('apply', null, InputOption::VALUE_NONE, 'Modifie, tag et commit les sources (sinon indique juste les changements qui seraient fait)')
            ->addOption('push', null, InputOption::VALUE_NONE, 'Push les modifications (apply ne push pas tout seul)')
            ->addOption('push-dry', null, InputOption::VALUE_NONE, 'Test le push les modifications uniquement')
            ->addOption('no-checkout', null, InputOption::VALUE_NONE, 'Ne pas relancer checkout si le répertoire SPIP existe déjà.')
            ->addOption('no-changelog', null, InputOption::VALUE_NONE, 'Ignore le test de changelog (probablement pratique pour une toute nouvelle branche).')
            ->addOption('with-plugins-json', null, InputOption::VALUE_NONE, 'Force le test de plugins-dist.json (qui n’est par défaut pas appliqué sur master).')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->init($input, $output);
        
        $branch = $input->getArgument('branch');
        $this->io->title("Release SPIP $branch");
        $this->config = new Config($branch);
        $this->release();

        return COMMAND::SUCCESS;
    }


    protected function preparer() : bool {
        $this->io->section("Prepare SPIP");

        $ok = $this->presenterSpipActuel();
        if (!$ok) {
            return false;
        }

        return $this->adapterSpip();
    }


    protected function presenterSpipActuel() : bool {
        $errs = [];
    
        $spip = new Spip($this->config->getDir());
        $version = $spip->getSpipVersion();
        $tag_version = $spip->getSpipTag($version);
        $schema = $spip->getSpipVersionBase();
        $schema_int = str_replace('_', '', $schema);

        $this->io->text("Versions actuelles :");
        if (false === stripos($version, 'dev')) {
            $this->io->text("- spip_version_branche : <comment>$version</comment>");
        } else {
            $this->io->fail("spip_version_branche : <comment>$version</comment>");
            $errs[] = "On ne peut releaser une version nommée 'dev'";
        }
        $this->io->text("- spip_version_code : <comment>" . $spip->getSpipVersionCode() . "</comment>");
        $this->io->text("- spip_version_base : <comment>" . $schema . "</comment>");
        if ($spip->getSpipVersionId() === $spip->getSpipVersionIdFromVersion($version)) {
            $this->io->text("- _SPIP_VERSION_ID : <comment>" . $spip->getSpipVersionId() . "</comment>");
        } else {
            $this->io->fail("- _SPIP_VERSION_ID : <comment>" . $spip->getSpipVersionId() . "</comment>");
            $errs[] = "_SPIP_VERSION_ID devrait être : " . $spip->getSpipVersionIdFromVersion($version);
        }
        if ($spip->getSpipVersionExtra() === $spip->getSpipVersionExtraFromVersion($version)) {
            $this->io->text("- _SPIP_EXTRA_VERSION : <comment>" . $spip->getSpipVersionExtra() . "</comment>");
        } else {
            $this->io->fail("- _SPIP_EXTRA_VERSION : <comment>" . $spip->getSpipVersionExtra() . "</comment>");
            $errs[] = "_SPIP_EXTRA_VERSION devrait être : '" . $spip->getSpipVersionExtraFromVersion($version) . "'";
        }

        try {
            $this->io->fail("_DEV_VERSION_SPIP_COMPAT : <comment>" . $spip->getSpipVersionDevCompat() . "</comment>");
            $errs[] = "_DEV_VERSION_SPIP_COMPAT existe ; il devrait être commenté.";
        } catch (ReleaseException $e) {
            $this->io->check("_DEV_VERSION_SPIP_COMPAT : inexistant ou commenté.");
        }

        $paquet_version = $spip->getPaquetVersion();
        $paquet_schema = $spip->getPaquetSchema();
        $this->io->text("\nVersions dans paquet.xml :");
        $this->io->test($paquet_version === $version, "Paquet version : <comment>$paquet_version</comment>");
        $this->io->test($paquet_schema === $schema_int, "Paquet schema : <comment>$paquet_schema</comment>");
        if ($paquet_version !== $version) {
            $errs[] = "La version du paquet.xml est différente de $version";
        }
        if ($paquet_schema !== $schema_int) {
            $errs[] = "Le schema du paquet.xml est différent de $schema_int";
        }

        $git = new Git($this->config->getDir());
        $this->io->text("\nTag présents sur HEAD :");
        $tags = $git->listTagsOnCommit();
        if ($tags) {
            foreach ($tags as $tag) {
                if ($tag === $tag_version) {
                    $this->io->check("Tag : <comment>$tag</comment>");
                } else {
                    $this->io->text("- Tag : <comment>$tag</comment>");
                }
            }
        } else {
            $this->io->fail("Aucun");
        }

        if (!in_array($tag_version, $tags)) {
            $this->io->text("\nTag <comment>$tag_version</comment> antérieur :");
            if ($err = $this->testTag($git, $tag_version)) {
                $errs[] = $err;
            }
        }

        // Gérer le fichier plugins-dist.json (hors master)
        if (!$this->config->isMaster()  or $this->input->getOption('with-plugins-json')) {
            $jsonFile = $this->config->getDir() . '/plugins-dist.json';
            $oldJson = file_get_contents($jsonFile);
            $json = $this->proposerPluginsDistJson();
            if ($oldJson === $json) {
                $this->io->check("plugins-dist.json semble à jour.");
            } else {
                file_put_contents($jsonFile, $json);
                $this->io->care("plugins-dist.json actualisé !");
                $diff = $git->diff('plugins-dist.json');
                $this->io->text($diff);
                $errs[] = "Plugins-dist.json à commiter.";
            }
        }

        // Gérer le changelog
        if ($errors = $this->checkChangelog($spip, $version)) {
            $errs = [...$errs, ...$errors];
        }


        $diff = $git->diff();
        if ($diff) {
            $this->io->text("\n<comment>Diff sur le git !</comment>");
            $this->io->text($diff);
            $errs[] = "Des modifications non commitées sont présentes.";
        }

        $this->io->note(
            "Vous devez, si ce n’est pas fait, éditer inc_version.php et paquet.xml"
            . " pour la nouvelle version et créer un commit du changement."
            . " Il n’est pas nécessaire de pusher le commit aussitôt ou de créer le tag correspondant.\n\n"
            . "code " . $spip->getPathSpipVersion() . "\n"
            . "code " . $spip->getPathPaquet() . "\n"
        );

        if ($errs) {
            $this->io->error($errs);
            throw new ReleaseException("Des erreurs à corriger empêchent la poursuite...");
        }

        return $this->io->askQuestion(new ConfirmationQuestion("Poursuivre avec ce contenu ?"));
    }


    protected function adapterSpip() {
        $spip = new Spip($this->config->getDir());
        $version = $spip->getSpipVersion();
        $tag_version = $spip->getSpipTag($version);
        $git = new Git($this->config->getDir());
        
        if ($git->hasTag($tag_version)) {
            // on a vérifié en amont que le tag est sur notre HEAD
            $this->io->check("Le tag <comment>$tag_version</comment> existe");
        } else {
            $this->createTag($git, $tag_version);
        }
        return true;
    }

    protected function proposerPluginsDistJson() {
        $list = [];
        $root = $this->config->getDir() . '/';
        $plugins = $this->config->getPluginsPath();
        foreach ($plugins as $plugin) {
            $directory = rtrim($plugin->getDirectory(), '/');
            $data = $this->decrirePluginPourJson($directory, $root);
            $name = basename($directory);
            if ($name === 'squelettes-dist') {
                $name = 'dist';
            }
            $list[$name] = $data;
        }

        return json_encode($list, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }


    function decrirePluginPourJson(string $directory, string $root): array {
        $config = parse_ini_file($directory . '/.git/config');
        $source = str_replace('git@git.spip.net:', 'https://git.spip.net/', $config['url']);
        $git = new Git($directory);
        $branch = $git->currentBranch();
        $item = [
            'path' => str_replace($root, '', $directory),
            'source' => $source,
            'branch' => $branch,
        ];
        $tag = $git->currentTagVersion();
        if ($tag) {
            $item['tag'] =  $tag;
        }
        return $item;
    }




    protected function push() : bool {
        $this->io->title("Push !");
        $ok = $this->io->askQuestion(new ConfirmationQuestion("Prêt ?"));
        if (!$ok) {
            return false;
        }

        $ok = $this->pushSPIP();
        if (!$ok) {
            return false;
        }

        return true;
    }

    protected function pushSPIP() : bool {
        $this->io->section("Push SPIP");
        $git = new Git($this->config->getDir());
        return $this->pushGit($git);
    }
}
