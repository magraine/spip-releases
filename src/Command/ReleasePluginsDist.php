<?php

namespace Spip\Tools\Releases\Command;

use Spip\Tools\Releases\Config;
use Spip\Tools\Releases\Edit\Git;
use Spip\Tools\Releases\Edit\Spip;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Spip\Tools\Releases\Edit\Plugin;
use Spip\Tools\Releases\ReleaseException;

class ReleasePluginsDist extends Release {

    protected function configure() {
        $this
            ->setName('release:plugins-dist')
            ->setDescription('Prépare une release des plugins-dist d’une branche de SPIP')
            ->addArgument('branch', InputArgument::OPTIONAL, 'Branche à releaser', 'master')
            ->addOption('apply', null, InputOption::VALUE_NONE, 'Modifie, tag et commit les sources (sinon indique juste les changements qui seraient fait)')
            ->addOption('push', null, InputOption::VALUE_NONE, 'Push les modifications (apply ne push pas tout seul)')
            ->addOption('push-dry', null, InputOption::VALUE_NONE, 'Test le push les modifications uniquement')
            ->addOption('no-changelog', null, InputOption::VALUE_NONE, 'Ignore le test de changelog (probablement pratique pour une toute nouvelle branche).')
            ->addOption('no-checkout', null, InputOption::VALUE_NONE, 'Ne pas relancer checkout si le répertoire SPIP existe déjà.')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->init($input, $output);
        
        $branch = $input->getArgument('branch');
        $this->io->title("Release Plugins dist SPIP $branch");
        $this->config = new Config($branch);
        $this->release();

        return COMMAND::SUCCESS;
    }


    protected function preparer() : bool {
        $this->io->section("Préparation");

        $ok = $this->presenterSpipActuel();
        if (!$ok) {
            return false;
        }

        $spip = new Spip($this->config->getDir());
        $version = $spip->getSpipVersion();

        return $this->adapterPluginsDist($version);

    }

    protected function adapterPluginsDist(string $spip_version) {
        $this->io->title("Prepare Plugins Dist");

        $compats = [
            $this->generateCompatSpip($spip_version, 0, true),
            $this->generateCompatSpip($spip_version, 0),
            $this->generateCompatSpip($spip_version),
            'Autre',
        ];

        $compat = $this->io->askQuestion(new ChoiceQuestion("Quelle compat SPIP pour les plugins ?", $compats));
        if ($compat === 'Autre') {
            $compat = $this->io->askQuestion(new Question("Quelle compat SPIP pour les plugins ?", $compats[1]));
        }
        $this->io->writeln("Compat plugins choisie: <info>$compat</info>");

        foreach ($this->config->getPluginsPath() as $plugin) {
            $this->preparerPluginWithRetry($plugin, $spip_version, $compat);
        }

        return true;
    }


    protected function presenterSpipActuel() : bool {
        $errs = [];
    
        $spip = new Spip($this->config->getDir());
        $version = $spip->getSpipVersion();
        $tag_version = $spip->getSpipTag($version);

        $this->io->text("Version SPIP : <comment>$version</comment>");

        $git = new Git($this->config->getDir());
        $this->io->text("\nTag présents sur HEAD :");
        $tags = $git->listTagsOnCommit();
        if ($tags) {
            foreach ($tags as $tag) {
                if ($tag === $tag_version) {
                    $this->io->check("Tag : <comment>$tag</comment>");
                } else {
                    $this->io->text("- Tag : <comment>$tag</comment>");
                }
            }
        } else {
            $this->io->fail("Aucun");
        }

        return $this->io->askQuestion(new ConfirmationQuestion("Poursuivre avec ce SPIP ?"));
    }

    protected function push() : bool {
        $this->io->title("Push Plugins-dist");
        foreach ($this->config->getPluginsPath() as $plugin) {
            $continue = $this->pushPlugin($plugin);
            if (!$continue) {
                return false;
            }
        }

        return true;
    }

    protected function pushPLugin(Plugin $plugin) : bool {
        $directory = $plugin->getDirectory();
        $this->io->section("Push " . basename($directory) . "\n");
        $git = new Git($directory);
        return $this->pushGit($git);
    }

}
