<?php

namespace Spip\Tools\Releases\Edit;

use Spip\Tools\Releases\ReleaseException;

class Git {
    /** @var string */
    protected $directory;

    public function __construct(string $directory) {
        if (!is_dir($directory)) {
            throw new ReleaseException("Directory $directory doesn’t exists.");
        }
        $this->directory = rtrim($directory, '/');
        if (!is_file($directory . '/.git/config')) {
            throw new ReleaseException("$directory has no .git/config inside.");
        }
    }

    public function diff(?string $file = null) : ?string {
        return $file ? $this->execute("diff $file") : $this->execute("diff");
    }

    public function hasTag(string $tag) : bool {
        $res = $this->execute("tag -l \"$tag\"");
        if ($res) {
            return true;
        }
        return false;
    }

    public function createTag(string $tag) : bool {
        $res = $this->execute("tag -a $tag -m \"$tag\"");
        return $this->hasTag($tag);
    }

    public function listTagsOnCommit(string $commit = 'HEAD'): array {
        $res = $this->execute("tag --points-at $commit");
        if (!$res) {
            return [];
        }
        return array_filter(explode("\n", $res));
    }

    /** Retourne un tag sur le commit qui contient un 'v' (vX.Y.Z) */
    public function currentTagVersion(string $commit = 'HEAD'): ?string {
        $tags = $this->listTagsOnCommit($commit);
        $tags = array_values(array_filter($tags, fn($t) => $t[0] === 'v' || is_numeric($t[0])));
        return $tags[0] ?? null;
    }

    public function add(string $file) {
        $cmd = "add $file";
        return $this->execute($cmd);
    }

    public function commit(string $message) {
        $cmd = "commit -m \"$message\"";
        return $this->execute($cmd);
    }

    public function sha(string $commit = 'HEAD') {
        $cmd = "rev-parse $commit";
        $sha = $this->execute($cmd);
        return trim($sha);
    }

    public function currentBranch(string $commit = 'HEAD') {
        $cmd = "rev-parse --abbrev-ref $commit";
        $branch = $this->execute($cmd);
        return trim($branch);
    }

    public function prettyLogBetween(string $from = '', string $to = 'HEAD'): string {
        // avec git v2.25+ on pourrait utiliser %al (email sans @domain.tld)
        // certain·e·s ont le nom, d’autres l’email... pas toujours les deux...
        $cmd = "log --pretty='%h | %ad | %<(20,trunc)%al | %<(20,trunc)%an | %s' --date=short $from..$to";
        return $this->execute($cmd) ?? '';
    }

    public function prettyLogBetweenShort(string $from = '', string $to = 'HEAD'): string {
        // avec git v2.25+ on pourrait utiliser %al (email sans @domain.tld)
        // certain·e·s ont le nom, d’autres l’email... pas toujours les deux...
        $cmd = "log --pretty='%h | %ad | %<(20,trunc)%al | %<(20,trunc)%an | %<(120,trunc)%s' --date=short $from..$to";
        return $this->execute($cmd) ?? '';
    }

    public function lg(string $params = "-10"): string {
        $cmd = "log --graph --pretty=tformat:'%Cred%h%Creset -%C(yellow)%d%Creset %<(100,trunc)%s %Cgreen(%an %cr)%Creset' --abbrev-commit --date=relative";
        $cmd .= " $params";
        return $this->execute($cmd) ?? '';
    }

    public function lg_between(string $from = '', string $to = 'HEAD'): string {
        if (!$from) {
            $from = 'origin/' . $this->currentBranch();
        }
        return $this->lg("$from..$to");
    }

    public function log_first_sha(string $file = ""): string {
        $cmd = trim("log --pretty=tformat:'%h' -1 $file");
        $sha = $this->execute($cmd) ?? '';
        return trim($sha);
    }

    public function push(bool $dryrun = false, bool $verbose = false) {
        $dryrun = ($dryrun ? '--dry-run' : '');
        $verbose = ($verbose ? '--verbose' : '');
        $cmd = trim("push $dryrun $verbose");
        return $this->execute($cmd);
    }

    public function pushTags(bool $dryrun = false, bool $verbose = false) {
        $dryrun = ($dryrun ? '--dry-run' : '');
        $verbose = ($verbose ? '--verbose' : '');
        $cmd = trim("push --tags $dryrun $verbose");
        return $this->execute($cmd);
    }

    public function execute(string $command) {
        $cmd = "cd $this->directory && git $command";
        return shell_exec($cmd);
    }
}