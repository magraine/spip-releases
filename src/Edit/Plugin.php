<?php

namespace Spip\Tools\Releases\Edit;

use Spip\Tools\Releases\Config;
use Spip\Tools\Releases\ReleaseException;
use SimpleXMLElement;

class Plugin {

    /** @var string */
    protected $directory;

    /** @var string */
    protected $paquet = 'paquet.xml';

    private Git $git;

    public function __construct(string $directory) {
        if (!is_dir($directory)) {
            throw new ReleaseException("Directory $directory doesn’t exists.");
        }
        $this->directory = rtrim($directory, '/') . '/';
    }

    public function getDirectory(): string {
        return $this->directory;
    }

    public function getGit(): Git {
        $this->git ??= new Git($this->directory);
        return $this->git;
    }

    public function getChangelog(): Changelog {
        return new Changelog($this->getDirectory(), $this->getGit());
    }

    public function getExistantChangelog(): ?Changelog {
        $changelog = $this->getChangelog();
        if ($changelog->hasChangelogFile()) {
            return $changelog;
        }
        /** Will be drop when SPIP evolves with only CHANGELOG.md on all supported versions (4.1+) */
        $changelogTxt = new ChangelogTxt($this->getDirectory(), $this->getGit());
        if ($changelogTxt->hasChangelogFile()) {
            return $changelogTxt;
        }
        return null;
    }

    public function getPaquetVersion(): string {
        return $this->getAttrValue($this->getContentPaquet(), 'version');
    }
    public function getPaquetEtat(): string {
        return $this->getAttrValue($this->getContentPaquet(), 'etat');
    }
    public function getPaquetSchema(): string {
        return $this->getAttrValue($this->getContentPaquet(), 'schema');
    }

    public function getPaquetCompat(): string {
        return $this->getAttrValue($this->getContentPaquet(), 'compatibilite');
    }

    public function getPathPaquet(): string {
        return $this->directory . $this->paquet;
    }

    public function getContentPaquet(): string {
        $file = $this->directory . $this->paquet;
        if (!\is_file($file)) {
            throw new ReleaseException("$file not found");
        }
        
        return \file_get_contents($file);
    }

    public function getPluginTag(string $version, bool $has_prefix = false): string {
        return ($has_prefix ? 'v' : '') . $version;
    }

    public function setPaquetVersion(string $version): bool {
        $file = $this->directory . $this->paquet;
        $content = $this->getContentPaquet();

        if (!$new = \preg_replace('/(?<=version=")(.+)(?=")/', $version, $content, 1)) {
            $this->io->error("Version not replaced.");
            return false;
        }
        \file_put_contents($file, $new);
        return $this->getPaquetVersion() === $version;
    }

    public function setPaquetEtat(string $etat): bool {
        $file = $this->directory . $this->paquet;
        $content = $this->getContentPaquet();

        if (!$new = \preg_replace('/(?<=etat=")(.+)(?=")/', $etat, $content, 1)) {
            $this->io->error("Etat not replaced.");
            return false;
        }
        \file_put_contents($file, $new);
        return $this->getPaquetEtat() === $etat;
    }

    public function setPaquetCompat(string $compat) : bool {
        $file = $this->directory . $this->paquet;
        $content = $this->getContentPaquet();

        if (!$new = \preg_replace('/(?<=compatibilite=")(.+)(?=")/', $compat, $content, 1)) {
            $this->io->error("Compat not replaced.");
            return false;
        }
        \file_put_contents($file, $new);
        return $this->getPaquetCompat() === $compat;
    }

    public function getName(): string {
        return $this->getTagContent($this->getContentPaquet(), 'nom');
    }

    /**
     * Incrémente le .z d’une version
     *
     * x.y.z
     * x.y.0-dev
     * 
     * @param string $version
     * @return string
     */
    public function incrementVersion(string $version): string {
        $version = explode('.', $version);
        $last = array_pop($version);
        if (intval($last) != $last) {
            $last = intval($last); // 0-dev -> 0
        } else {
            $last++;
        }
        $version[] = $last;
        return implode('.', $version);
    }

    /**
     * Décrémente le .z d’une version, 
     * sauf si 0 : returne null
     *
     * @param string $version
     * @return string|null
     */
    public function tryDecrementVersion(string $version): ?string {
        $version = explode('.', $version);
        $last = array_pop($version);
        if (is_numeric($last) and $last > 0) {
            $last--;
            $version[] = $last;
            return implode('.', $version);
        }
        return null;
    }

    protected function getAttrValue(string $content, string $attr): string {
        if (!\preg_match('/^\s*' . $attr . '\s?=\s?"(.+)"$/m', $content, $m)) {
            throw new ReleaseException("Attr $attr not found");
        }
        return $m[1];
    }


    protected function getTagContent(string $content, string $tag): string {
        $xml = new \SimpleXMLElement($content);
        if (!isset($xml->$tag)) {
            throw new ReleaseException("Tag $tag not found");
        }
        return (string) $xml->$tag;
    }


    /** cherche la valeur de $truc = valeur; dans un contenu */
    protected function getVarValue(string $content, string $var): string {
        $expr = '/^\$' . $var . '\s?=(.+);$/m';
        if (!\preg_match($expr, $content, $m)) {
            throw new ReleaseException("$var not found");
        }
        return trim($m[1], "'\" ");
    }

    /** cherche la valeur de define('const', xx); dans un contenu */
    protected function getConstValue(string $content, string $const): string {
        $expr = '/^define\(\'' . $const . '\',(.+)\);$/m';
        if (!\preg_match($expr, $content, $m)) {
            throw new ReleaseException("$const not found");
        }
        return trim($m[1], "'\" ");
    }

    public function trouverTagPrecedent(string $version_precedente): array {
        if ($this->git->hasTag($this->getPluginTag($version_precedente, false))) {
            return [
                'version' => $this->getPluginTag($version_precedente, false), 
                'prefix' => false
            ];
        } elseif ($this->git->hasTag($this->getPluginTag($version_precedente, true))) {
            return [
                'version' => $this->getPluginTag($version_precedente, true), 
                'prefix' => true
            ];
        }
        return [];
    }
}