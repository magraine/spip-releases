<?php

namespace Spip\Tools\Releases\Edit;
use Spip\Tools\Releases\ReleaseException;

class Changelog {
    protected const CHANGELOG = 'CHANGELOG.md';

    protected string $directory;
    protected Git $git;

    protected ?string $changelogContent = null;

    public function __construct(string $directory, ?Git $git = null) {
        $this->directory = $directory;
        $this->git = $git ?? new Git($directory);
    }

    public function hasChangelogFile(): bool {
        $file = $this->getPathChangelog();
        return is_file($file);
    }

    public function getPathChangelog(): string {
       return $this->directory . static::CHANGELOG;
    }

    public function hasChangelogVersion(string $version): bool {
        $content = $this->getContentChangelog();
        // 4.3.0, mais pas 4.3.0-beta
        $search = "## $version";
        if ($version !== 'Unreleased') {
            $search .= ' ';
        }
        return false !== \stripos($content, $search);
    }

    public function hasUnreleasedSection(): bool {
        return $this->hasChangelogVersion('Unreleased');
    }

    protected function readContentChangelog(): string {
        $file = $this->getPathChangelog();
        if (!\is_file($file)) {
            $this->changelogContent = '';
        } else {
            $this->changelogContent = \file_get_contents($file);
            $this->changelogContent = $this->correctBracketTitle($this->changelogContent);
        }
        return $this->changelogContent;
    }

    private function correctBracketTitle(string $changelogContent): string {
        return preg_replace('@\n## \[(.+)\]@', "\n" . '## $1', $changelogContent);
    }

    protected function getContentChangelog(): string {
        if ($this->changelogContent === null) {
            $this->changelogContent = $this->readContentChangelog();
        }
        return $this->changelogContent;
    }

    protected function extractChangelogVersion(string $from, string $to): string {
        $changelog = $this->getContentChangelog();
        if (!$changelog) {
            return '';
        }
        // si unreleased, de unreleased à $from (sinon à fin)…
        // sinon de $to à $from (sinon à fin)…
        if ($this->hasChangelogVersion('Unreleased')) {
            $to = 'Unreleased';
        }

        $from = ltrim($from, 'v');
        $start = preg_quote("\n## $to");
        $next = preg_quote("\n## $from");
        if (!$this->hasChangelogVersion($from)) {
            $next = '$';
        }
        $pattern = "/((?:$start).*)(?:$next)/is";
        if (preg_match($pattern, $changelog, $m)) {
            return trim($m[1]);
        }
        return '';
    }

    /** @param Plugin[] $plugins */
    public function forSpip(string $from, string $to, Plugin $spip, array $plugins, bool $show_commits = true): array {
        $changelog = $this->forPlugin($from, $to, $spip, $show_commits);
        foreach ($plugins as $plugin) {
            $changes = $this->forPluginDistChangelog($from, $to, $spip, $plugin);
            $commits = [];
            if ($show_commits) {
                $commits = $this->forPluginDistCommits($from, $to, $spip, $plugin);
            }
            if ($changes or $commits) {
                if ($commits) {
                    $commits = [($changes ? "\n" : '') . "## Commits\n", ...$commits];
                }
                $changelog = [
                    ...$changelog, 
                    "\n\n\n# " . $plugin->getName() . "\n",
                    ...$changes,
                    ...$commits,
                ];
            }
        }

        return $changelog;
    }

    public function forPlugin(string $from, string $to, Plugin $plugin, bool $show_commits = true): array {
        $extracts = $this->forPluginChangelog($from, $to);
        $commits = [];
        if ($show_commits) {
            $commits = $this->forPluginCommits($from, $to);
        }
        $name = $plugin->getName();
        return [
            "# $name\n", 
            ...$extracts,
            ($extracts ? "\n" : '') . "\n## Commits", 
            ...$commits,
        ];
    }

    public function forPluginDistChangelog(string $from, string $to, Plugin $spip, Plugin $plugin): array {

        $content = [];

        $old_json = $this->getPluginsDistJsonFrom($from);
        if (!$old_json) {
            return $content;
        }

        $dirname = basename($plugin->getDirectory());
        $json_name = $dirname === 'squelettes-dist' ? 'dist' : $dirname;
        $changelog = new Changelog($plugin->getDirectory());
        // on suppose que la version a déjà été mise à jour dans le paquet… hum…
        $log = $changelog->extractChangelogVersion($old_json[$json_name]['tag'], $plugin->getPaquetVersion());
        return $log ? [$log] : [];

        if ($log) {
            $content = [...$content, "\n# Changelog $name\n", $log];
        }

        return $content;
    }

    public function forPluginDistCommits(string $from, string $to, Plugin $spip, Plugin $plugin): array {
        $content = [];

        $old_json = $this->getPluginsDistJsonFrom($from);
        if (!$old_json) {
            return $content;
        }

        $dirname = basename($plugin->getDirectory());
        $json_name = $dirname === 'squelettes-dist' ? 'dist' : $dirname;
     
        $log = $this->getPrettyLog(new Git($plugin->getDirectory()), $old_json[$json_name]['tag']);
        return $log;
    }


    public function forPluginChangelog(string $from, string $to): array {
        $plugin = $this->extractChangelogVersion($from, $to);
        return $plugin ? [$plugin] : [];
    }


    public function forPluginCommits(string $from, string $to): array {
        $plugin = $this->showLogsBetween($from, $to, '');
        return $plugin;
    }

    public function showLogsBetween(string $from, string $to = 'Unreleased', ?string $titre = null): array {
        $content = $this->showHeaderCommits($from, $to, $titre);
        $vfrom = 'v' . ltrim($from, 'v');
        if ($this->git->hasTag($from)) {
            $log = $this->getPrettyLog($this->git, $from);
        } elseif ($this->git->hasTag($vfrom)) {
            $log = $this->getPrettyLog($this->git, $vfrom);
        } else {
            $log = ['???? Pas de tag ????'];
        }
        return [...$content, ...$log];
    }

    public function setUnreleasedToVersion(string $version): bool {
        if ($this->hasUnreleasedSection()) {
            $content = $this->readContentChangelog();
            $date = (new \DateTime())->setTimestamp(time())->format('Y-m-d');
            $unerelased = "## Unreleased";
            $released = "## $version - $date";
            $content = str_replace($unerelased, $released, $content);
            file_put_contents($this->getPathChangelog(), $content);
            return true;
        }
        return false;
    }


    protected function getPluginsDistJsonFrom(string $from): array {
        $from = ltrim($from, 'v');
        $vfrom = 'v' . $from;
        if ($this->git->hasTag($from)) {
            $old_json = $this->git->execute("show $from:plugins-dist.json");
        } elseif ($this->git->hasTag($vfrom)) {
            $old_json = $this->git->execute("show $vfrom:plugins-dist.json");
        } else {
            return [];
        }

        $old_json = json_decode($old_json, true);
        return $old_json;
    }

    protected function showHeader(string $from, string $to, ?string $titre = null): array {
        $date = (new \DateTime())->setTimestamp(time())->format('Y-m-d');
        $lines = [];
        $lines[] = "\n## $to - $date\n";
        return $lines;
    }

    protected function showHeaderCommits(string $from, string $to, ?string $titre = null): array {
        $date = (new \DateTime())->setTimestamp(time())->format('Y-m-d');
        $lines = [];
        $lines[] = "\n### $to - $date\n";
        return $lines;
    }


    protected function getPrettyLog(Git $git, string $from, string $to = 'HEAD'): array {
        $logs = $git->prettyLogBetween($from, $to);
        $logs = $this->parseGitLogs($logs);
        foreach ($logs as $k => $log) {
            $logs[$k] = "- {$log->commit} {$log->date} {$log->message} ($log->auteur)";
        }
        return $logs;
    }

    /** @return Log[] */    
    protected function parseGitLogs(string $logs): array {
        $logs = \explode("\n", $logs);
        $logs = \array_map("trim", $logs);
        $logs = \array_filter($logs);
        // commit | date | email | nom | texte
        foreach($logs as $k => $line) {
            $line = \explode("|", $line, 5);
            $line = array_map('trim', $line);
            [$commit, $date, $auteur, $nom, $log] = $line;
            if (!$auteur) {
                $auteur = \explode("@", $nom, 2);
                $auteur = rtrim($auteur[0]);
            }
            $logs[$k] = new Log(
                commit: $commit, 
                date: $date, 
                auteur: $auteur, 
                message: $log,
            );
        }
        return $logs;
    }
}