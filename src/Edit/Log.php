<?php

namespace Spip\Tools\Releases\Edit;
use Spip\Tools\Releases\ReleaseException;

class Log {

    public function __construct(
        public readonly string $commit,
        public readonly string $date,
        public readonly string $auteur,
        public readonly string $message,
    ) {

    }

}