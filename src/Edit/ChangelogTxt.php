<?php

namespace Spip\Tools\Releases\Edit;
use Spip\Tools\Releases\ReleaseException;

/**
 * Ce fichier à vocation à disparaitre
 * Lorsque SPIP 3.2 et 4.0 ne seront plus maintenues 
 * (plus besoin de gérer un changelog.txt)
 */
class ChangelogTxt extends Changelog {

    protected const CHANGELOG = 'CHANGELOG.txt';

    public function hasChangelogVersion(string $version): bool {
        $content = $this->getContentChangelog();
        $search = " -> v$version ";
        if (false !== \stripos($content, $search)) {
            return true;
        }
        $search = " -> spip-$version ";
        return (false !== \stripos($content, $search));
    }

    public function hasUnreleasedSection(): bool {
        return false;
    }

    /** @param Plugin[] $plugins */
    public function forSpip(string $from, string $to, Plugin $spip, array $plugins): array {
        $changelog = $this->forPlugin($from, $to, $spip);
        foreach ($plugins as $plugin) {
            $commits = $this->forPluginDistCommits($from, $to, $spip, $plugin);
            if ($commits) {
                if ($commits) {
                    $commits = ["## Commits\n", ...$commits];
                }
                $changelog = [
                    ...$changelog, 
                    "\n\n# " . $plugin->getName() . "\n",
                    ...$commits,
                ];
            }
        }

        return $changelog;
    }

    public function forPlugin(string $from, string $to, Plugin $plugin): array {
        return $this->forPluginCommits($from, $to);
    }

    /**
     * @param Plugin[] $plugins
     */
    public function showLogsPluginsBetween(array $plugins, string $from, string $to = 'Unreleased', ?string $titre = null): array {
        $content = $titre ? $this->showHeader($from, $to, $titre) : [];

        $old_json = $this->git->execute("show $from:plugins-dist.json");
        $old_json = json_decode($old_json, true);
        # old fashion way of tags… @deprecated
        $tag_spip_version_precedente = 'spip/' . $from;
       
        $lengths = array_map(function($plugin) {
            return strlen(basename($plugin->getDirectory()));
        }, $plugins);
        $max_len = max($lengths);

        foreach ($plugins as $_plugin) {
            $name = basename($_plugin->getDirectory());
            $_name = $name === 'squelettes-dist' ? 'dist' : $name;

            $log = $this->getPrettyLog(new Git($_plugin->getDirectory()), $old_json[$_name]['tag'] ?? $tag_spip_version_precedente);
            if ($log) {
                $log = array_map(function($line) use ($max_len, $name) {
                    return str_pad("$name", $max_len + 1) . '| ' . $line;
                }, $log);
                $content = [...$content, ...$log];
            }
        }
        return $content;
    }

    protected function showHeader(string $from, string $to, ?string $titre = null): array {
        // Entête
        # SPIP-Core spip-3.2.5 -> spip-3.2.6 (10 décembre 2019)
        $date = (new \DateTime())->setTimestamp(time())->format('d F Y');
        $titre ??= '';

        $lines = [];
        $lines[] = "";
        $lines[] = trim("$titre $from -> $to ($date)");
        $lines[] = str_pad("", strlen($lines[1]), "-");
        $lines[] = "";
        return $lines;
    }

    protected function getPrettyLog(Git $git, string $from, string $to = 'HEAD'): array {
        $logs = $git->prettyLogBetweenShort($from, $to);
        $logs = $this->parseGitLogs($logs);

        $max_author_len = 13; // rastapopoulos ...
        foreach($logs as $k => $log) {
            $max_author_len = \max($max_author_len, strlen($log->auteur));
        }
        foreach($logs as $k => $log) {
            $auteur = \str_pad($log->auteur, $max_author_len + 1);
            $logs[$k] = \implode(" | ", [$log->commit, $auteur, $log->date, $log->message]);
        }
        return $logs;
    }

}