<?php

namespace Spip\Tools\Releases\Edit;

use Spip\Tools\Releases\Config;
use Spip\Tools\Releases\ReleaseException;

class Spip extends Plugin {

    protected $paquet = 'ecrire/paquet.xml';
    protected $inc_version = 'ecrire/inc_version.php';

    /*
        $spip_version_branche = "3.2.7";
        define('_SPIP_VERSION_ID', 30207);
        define('_SPIP_EXTRA_VERSION', '');
        define('_DEV_VERSION_SPIP_COMPAT',"3.2.99");
        $spip_version_code = 24447;
        $spip_version_base = 24379;
     */

    public function getSpipVersion() {
        return $this->getSpipVar('spip_version_branche');
    }

    public function getSpipVersionCode(): string {
        return $this->getSpipVar('spip_version_code');
    }

    public function getSpipVersionBase(): string {
        return $this->getSpipVar('spip_version_base');
    }

    public function getSpipVersionId(): string {
        return $this->getSpipConst('_SPIP_VERSION_ID');
    }

    public function getSpipVersionIdFromVersion(string $version): string {
        $version = explode('-', $version, 2); // [4.2.0, dev]
        $versions = explode('.', $version[0]); // [4, 2, 0]
        if ($versions[1] < 10) {
            $versions[1] = '0' . $versions[1];
        }
        if ($versions[2] < 10) {
            $versions[2] = '0' . $versions[2];
        }
        return implode('', $versions);
    }


    public function getSpipVersionExtraFromVersion(string $version): string {
        $version = explode('-', $version, 2); // [4.2.0, dev]
        return ($version[1] ?? '') ? '-' . $version[1] : '';
    }

    public function getSpipVersionExtra(): string {
        return $this->getSpipConst('_SPIP_EXTRA_VERSION');
    }

    public function getSpipVersionDevCompat(): string {
        return $this->getSpipConst('_DEV_VERSION_SPIP_COMPAT');
    }

    public function getPathSpipVersion(): string {
        return $this->directory . $this->inc_version;
    }

    public function getSpipTag(string $version): string {
        return $this->getPluginTag($version, true);
    }

    protected function getSpipVar(string $var): string {
        return $this->getVarValue($this->getContentSpipVersion(), $var);
    }

    protected function getSpipConst(string $const): string {
        return $this->getConstValue($this->getContentSpipVersion(), $const);
    }

    protected function getContentSpipVersion(): string {
        $file = $this->getPathSpipVersion();
        if (!\is_file($file)) {
            throw new ReleaseException("$file not found");
        }
        
        $content = \file_get_contents($file);
        return $content;
    }

}