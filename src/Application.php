<?php

namespace Spip\Tools\Releases;

use Symfony\Component\Console\Application as ConsoleApplication;

class Application extends ConsoleApplication {

    const NAME = "Spip Releases";
    const VERSION = "1.1.0";

    /**
     * Application constructor.
     *
     * @param array $options
     */
    public function __construct(array $options = []) {
        parent::__construct(self::NAME, self::VERSION);

        $this->add(new Command\ReleaseSpip());
        $this->add(new Command\ReleasePlugin());
        $this->add(new Command\ReleasePluginsDist());
        $this->add(new Command\ReleasePluginsCore());
    }
}