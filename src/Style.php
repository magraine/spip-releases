<?php

namespace Spip\Tools\Releases;

use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Ajout de helpers en suppléments des Styles Symfony
 */
class Style extends SymfonyStyle {

    public function test(bool $test, $message) {
        if ($test) {
            $this->check($message);
        } else {
            $this->fail($message);
        }
    }

    public function check($message) {
        $this->prependText($message, '<fg=green>✔</> ');
    }

    public function fail($message) {
        return $this->prependText($message, '<fg=red>✘</> ');
    }

    public function care($message) {
        return $this->prependText($message, '<fg=yellow;options=bold>!</> ');
    }

    public function prependText($message, $prepend) {
        $messages = is_array($message) ? array_values($message) : array($message);
        foreach ($messages as $message) {
            $this->text($prepend . $message);
        }
    }
}