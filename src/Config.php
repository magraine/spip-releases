<?php

namespace Spip\Tools\Releases;
use Spip\Tools\Releases\Edit\Plugin;

class Config {

    private string $outputDir = 'releases/';

    private string $branch;
    private string $plugin;

    /** @var Plugin[] */
    private ?array $list = null;

    public function __construct(string $branch, string $plugin = 'spip') {
        $this->branch = $branch;
        $this->plugin = $plugin;
    }

    public function getBranch(): string {
        return $this->branch;
    }

    public function getBranchName(): string {
        return $this->branch;
    }

    public function getPlugin(): string {
        return $this->plugin;
    }

    public function getDir(): string {
        return $this->outputDir . $this->plugin . DIRECTORY_SEPARATOR . $this->branch;
    }

    public function isMaster(): bool {
        return $this->branch === 'master';
    }

    /**
     * 'spip' specific
     * 
     * @return Plugin[]
     */
    public function getPluginsPath(): array {
        if ($this->list === null) {
            $this->list = [];
            $destination = $this->getDir();
            $jsonFile = $this->getDir() . '/plugins-dist.json';
            if (!file_exists($jsonFile)) {
                return $this->list;
            }
            $json = file_get_contents($jsonFile);
            $plugins = json_decode($json, true);
            foreach ($plugins as $plugin) {
                $dir = $destination . DIRECTORY_SEPARATOR . $plugin['path'];
                if (is_dir($dir)) {
                    $this->list[] = new Plugin($dir);
                }
            } 
            sort($this->list);
        }
        
        return $this->list;
    }
}