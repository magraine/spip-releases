# Spip release

## Usage

`composer release` is aliased to `php bin/spip-release`.
The command needs `--` to pass arguments and options.

### Releases

Il existe 4 scripts :

- `composer release` (spip sans plugins-dist)
- `composer release-plugins-dist` (plugins-dist de spip)
- `composer release-plugins-core` (plugins Core de spip — autres que plugins-dist)
- `composer release-plugin` (Un plugin core spécifique)

Qui sont respectivement des alias de :

- `php bin/spip-releases release:spip`
- `php bin/spip-releases release:plugins-dist`
- `php bin/spip-releases release:plugins-core`
- `php bin/spip-releases release:plugin`

Les scripts ont un premier argument commun (sauf `release:plugin`, en 2è argument) :

- `branch` : indique la branche à utiliser (défaut : master)

Les scripts ont des options communes :

- `--apply` : applique les changements proposés (sans push)
- `--push-dry` : test les push sur le serveur (sans les réaliser réellement)
- `--push` : push les modifs sur le serveur (fait un test dry néanmoins avant quand même)
- `--no-checkout` : ne re checkout pas le dépot


### Releaser SPIP

Ça consistera à lancer successivement les commandes :

```sh
# bourin : on croit que tout va marcher d'un coup
rm -rf releases/
composer release -- --apply --push
composer release-plugins-dist -- --apply --push
composer release-plugins-core -- --apply --push

# soft : on y va par étapes…
rm -rf releases/
composer release
composer release -- --no-checkout --apply
composer release -- --no-checkout --push-dry
composer release -- --no-checkout --push

composer release-plugins-dist
composer release-plugins-dist -- --no-checkout --apply
composer release-plugins-dist -- --no-checkout --push-dry
composer release-plugins-dist -- --no-checkout --push

composer release-plugins-core
composer release-plugins-core -- --no-checkout --apply
composer release-plugins-core -- --no-checkout --push-dry
composer release-plugins-core -- --no-checkout --push
```

Si on release une branche, ça ressemblera à :

```sh
# bourin : on croit que tout va marcher d'un coup
rm -rf releases/
composer release -- 3.2 --apply --push
composer release-plugins-dist -- 3.2 --apply --push
composer release-plugins-core -- 3.2 --apply --push
```

### Release SPIP, détail des options


```sh
# release spip master (aucune modif)
composer release
# release spip master (modifs et commits / tags)
composer release -- --apply
# release spip master (Teste le push des git (--dry-run) - ne push pas sur le serveur réellement)
composer release -- --push-dry
# release spip master (push les git)
# (le test --dry-run est tout de même fait auparavant pour confirmation)
composer release -- --push
# la totale :
composer release -- --apply --push

# release spip 3.2
composer release -- 3.2
# ne pas refaire le checkout.php si déjà fait
composer release -- --no-checkout
# ignore le test de CHANGELOG.txt
composer release -- --no-changelog
```

### Release un plugin Core (autre que plugin-dist)

```sh
# release organiseur master (aucune modif)
composer release-plugin -- organiseur
# pour indiquer la version de SPIP à utiliser par défaut
composer release-plugin -- organiseur --spip=4.0.0-beta
# release organiseur branche spip-3.2 (aucune modif)
composer release-plugin -- organiseur spip-3.2
# meme parametres sinon ; la totale
composer release-plugin -- organiseur --apply --push  --spip=4.0.0-beta
```

## Fonctionnement

Le script demande certaines interventions manuelles (particulièrement pour le core SPIP),
et vérifie la cohérence de ce qui est fait. 

Il est par défaut en mode simulation et ne modifie pas les sources.
Il faut appliquer l’option `--apply` pour appliquer les changements et commits.

Le script ne push rien par défaut.
Il faut appliquer l’option `--push` pour cela.
Le push vérifiera d’abord l’action avec des commandes `--dry-run` et demandera confirmation
pour chaque push.

## Le plan

On release une branche (master, 3.2, 3.1)...

- on checkout la branche voulue (releases/master, releases/3.2)

- pour SPIP
  - [humain] on modifie évuentuellement le numéro de version du SPIP
  - [humain] [hors master] on propose un changelog
  - [humain] on crée le commit (si changement de version et changelog)
  - on crée le tag de la version de spip

- pour chaque plugin
  - si master : on propose d’incrémenter .z la version du plugin (si commits depuis le dernier tag de version)
  - si master : on crée le tag de version plugin
  - tous : on crée le tag spip/{version}
